const wx_user = require("./wx_user");
const custom_group = require("./custom_group");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "custom",
    hasTimestamps: true,
    hidden: ["password_org"],
    wx_user: function() {
        return this.hasOne(wx_user, "custom_id");
    },
    group: function() {
        return this.belongsTo(custom_group, "group_id");
    }
});
