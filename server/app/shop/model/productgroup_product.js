const group = require("./productgroup");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_productgroup_product",
    hasTimestamps: true,
    group: function() {
        return this.belongsTo(group, "group_id");
    }
});
